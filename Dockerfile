FROM openjdk:8-jre-alpine
COPY target/*.jar /home/spring-clinic.jar
CMD ["java", "-jar","/home/spring-clinic.jar"]